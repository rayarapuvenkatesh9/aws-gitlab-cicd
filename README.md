# AWS GitLab CICD

This project contains templates for the creation of IAM Roles and an OIDC IDP within AWS Account(s) to support the authentication of GitLab-CI operations on AWS Account(s).

These roles are leveraged for full GitLab-CI Automation into AWS Accounts using the following pipeline projects:

- CloudFormation: [https://gitlab.com/rwickit/cicd/cfn](https://gitlab.com/rwickit/cicd/cfn)
- Terraform: [https://gitlab.com/rwickit/cicd/tf](https://gitlab.com/rwickit/cicd/tf)

## Configuration Information

GitLab CI/CD with OpenID Connect (OIDC) allows for secure and streamlined authentication and authorization for continuous integration and deployment pipelines. OIDC enables GitLab to authenticate users and authorize access to resources without the need for traditional username/password credentials.

### OIDC

Using OIDC identity provider is more secure than using static IAM access keys and secrets for CICD access from Gitlab. Some key benefits of OIDC over static keys are:

1. Fine-grained access control: With OIDC, we can create IAM roles with permissions tailored to the specific CICD workflow. Static keys provide root account access which is an overkill.
2. Short-lived credentials: OIDC provides temporary credentials that expire in a few hours. Static keys never expire and pose a risk if compromised.
3. Auditability: OIDC login events can be audited in CloudTrail. It is difficult to audit use of static keys.
4. Easier revocation: Compromised OIDC credentials can be easily revoked. Revoking static keys requires rotation of all resources using those keys.

### OIDC Deployment

To configure GitLab OIDC (OpenID Connect) authentication in AWS, you will need to follow these general steps:

1. Configure an OIDC provider in AWS: First, you will need to set up an OIDC provider in AWS. You can follow the instructions provided in the AWS documentation to set up an OIDC provider: **[https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_create_oidc.html](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_create_oidc.html)**
2. Obtain the OIDC provider metadata URL: Once you have set up the OIDC provider, you will need to obtain the metadata URL for the provider. You can find this URL in the AWS console, or you can use the AWS CLI to retrieve it.
3. Configure GitLab to use the OIDC provider: Next, you will need to configure GitLab to use the OIDC provider for authentication. You can follow the instructions provided in the GitLab documentation to configure GitLab to use OIDC authentication: **[https://docs.gitlab.com/ee/administration/auth/oidc.html#configure-gitlab](https://docs.gitlab.com/ee/administration/auth/oidc.html#configure-gitlab)**
4. Configure GitLab to trust the OIDC provider: Finally, you will need to configure GitLab to trust the OIDC provider by adding the provider's metadata URL and other relevant information to GitLab's configuration. You can follow the instructions provided in the GitLab documentation to configure GitLab to trust an OIDC provider: **[https://docs.gitlab.com/ee/administration/auth/oidc.html#configure-gitlab-to-trust-the-oidc-provider](https://docs.gitlab.com/ee/administration/auth/oidc.html#configure-gitlab-to-trust-the-oidc-provider)**

Here are some additional resources that may help you with configuring GitLab OIDC authentication in AWS:

- GitLab documentation on OIDC authentication: **[https://docs.gitlab.com/ee/administration/auth/oidc.html](https://docs.gitlab.com/ee/administration/auth/oidc.html)**
- AWS documentation on setting up an OIDC provider: **[https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_create_oidc.html](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_create_oidc.html)**
- AWS CLI documentation on retrieving the OIDC provider metadata URL: **[https://docs.aws.amazon.com/cli/latest/reference/iam/get-open-id-connect-provider.html](https://docs.aws.amazon.com/cli/latest/reference/iam/get-open-id-connect-provider.html)**

## Artifacts

The Templates in this project are:

- OIDC IDP Only ([idp-gitlab-oidc.yml](idp-gitlab-oidc.yml)) - Can be used by any future IAM roles where desired.
- Read-Only Role ([role-gitlab-read.yml](role-gitlab-read.yml)) - Used to read-only information from any account where deployed.
- Delivery Role ([role-gitlab-deliver.yml](role-gitlab-deliver.yml)) - Used to provide permissions to S3 buckets for delivery of deployment templates.
- Deploy Role ([role-gitlab-deploy.yml](role-gitlab-deploy.yml)) - Used run deploy operation within an account.
- Full Deployment ([role-gitlab-cicd.yml](role-gitlab-cicd.yml)) - Deploys the OIDC IDP and all Roles in a single deployment.

## Deployment

The deployment of these templates are required as a pre requirement to any future automation. Once these resources or deployed CICD can be run from individual GitLab Repositories.

These template(s) can be deployed individual to an account or from the management account to be delivered to every account in the organization.

> Note: The GitLab Group, Repo Names, and Role Names are Case Sensitive for STS to authenticate.

### CloudFormation Quick Launch

1. Login to AWS Account
2. Have this README.md open in same browser session
3. Click the `Launch Stack` button below to launch stack and supply parameters.

[![LaunchStack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://console.aws.amazon.com/cloudformation/home?region=us-east-2#/stacks/create/review?stackName=role-gitlab-cicd&templateURL=https://s3.us-east-2.amazonaws.com/solutions.rwick.it/gitlab-cicd/role-gitlab-cicd.yml)

### Manual Console Configuration

When deploying manually be sure to properly name and case parameters and names:

- Upload of template file:

```sh
stackname: role-gitlab-cicd
```

- S3 Object URL:

```sh
https://s3.us-east-2.amazonaws.com/solutions.rwick.it/gitlab-cicd/role-gitlab-cicd.yml
```

- S3 URI

```sh
s3://s3.us-east-2.amazonaws.com/solutions.rwick.it/gitlab-cicd/role-gitlab-cicd.yml
```

## Contribution

To make changes to this project, please follow appropriate protocol to track history.

1. Fork/Clone the Project: `git clone {{repo url}}`
2. Create your Feature Branch: `git checkout -b feature/AmazingFeature`
3. Commit your Changes: `git commit -m 'Add some AmazingFeature'`
4. Push to the Branch: `git push origin feature/AmazingFeature`
5. Open a Pull Request
6. If required, request review of PR and have a second individual Pull the change

### Road Map and Open Issues

See the [open issues](../../issues) for a list of proposed features (and known issues).

### Contact

View [project contributors](../../graphs/main) for more information.
