#!/bin/bash

#Validating the templates
echo "Validating the templates"
cfn-lint s3-bucket.yml
if [$? -eq 0 ];
then
  echo "Template is Valid"
else
  echo "Template is invalid"
fi
